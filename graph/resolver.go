// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.
package graph

import (
	"github.com/dancemystyle/backendchallenge/graph/model"
	"github.com/pkg/errors"
	"math/rand"
	"time"
)

type Resolver struct {
	meetings map[int]*model.Meeting
}

func NewResolver() *Resolver {
	return &Resolver{meetings: make(map[int]*model.Meeting)}
}

func (r *Resolver) GetMeetings() []*model.Meeting {
	meetings := make([]*model.Meeting, 0, len(r.meetings))
	for _, meeting := range r.meetings {
		meetings = append(meetings, meeting)
	}
	return meetings
}

func (r *Resolver) Create(input model.NewMeeting) (*model.Meeting, error) {
	startTime := input.StartTime
	if input.StartTime == nil {
		now := int(time.Now().Unix())
		startTime = &now
	}

	meeting := &model.Meeting{
		ID:          rand.Intn(10000),
		Title:       input.Title,
		Description: input.Description,
		StartTime:   startTime,
	}
	r.meetings[meeting.ID] = meeting
	return meeting, nil
}

func (r *Resolver) Update(input model.UpdateMeeting) (*model.Meeting, error) {
	meeting, ok := r.meetings[input.ID]
	if !ok {
		return nil, errors.Errorf("meeting %d not found", input.ID)
	}

	if input.Title != nil {
		meeting.Title = *input.Title
	}
	if input.Description != nil {
		meeting.Description = *input.Description
	}
	if input.StartTime != nil {
		meeting.StartTime = input.StartTime
	}
	r.meetings[input.ID] = meeting

	return meeting, nil
}

func (r *Resolver) Delete(input model.DeleteMeeting) (*model.Meeting, error) {
	meeting, ok := r.meetings[input.ID]
	if !ok {
		return nil, errors.Errorf("meeting %d not found", input.ID)
	}

	delete(r.meetings, input.ID)

	return meeting, nil
}