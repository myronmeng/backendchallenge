// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.
package graph

import (
	"context"

	"github.com/dancemystyle/backendchallenge/graph/generated"
	"github.com/dancemystyle/backendchallenge/graph/model"
)

func (r *mutationResolver) CreateMeeting(ctx context.Context, input model.NewMeeting) (*model.Meeting, error) {
	return r.Create(input)
}

func (r *mutationResolver) UpdateMeetings(ctx context.Context, input model.UpdateMeetingsInput) (*model.UpdateMeetingsResponse, error) {
	toAdd := make([]*model.Meeting, 0, len(input.MeetingsToAdd))
	for _, in := range input.MeetingsToAdd {
		meeting, err := r.Create(*in)
		if err != nil {
			return nil, err
		}
		toAdd = append(toAdd, meeting)
	}

	toUpdate := make([]*model.Meeting, 0, len(input.MeetingsToUpdate))
	for _, in := range input.MeetingsToUpdate {
		meeting, err := r.Update(*in)
		if err != nil {
			continue
		}
		toUpdate = append(toUpdate, meeting)
	}

	toDelete := make([]*model.Meeting, 0, len(input.MeetingsToDelete))
	for _, in := range input.MeetingsToDelete {
		meeting, err := r.Delete(*in)
		if err != nil {
			continue
		}
		toDelete = append(toDelete, meeting)
	}

	return &model.UpdateMeetingsResponse{
		MeetingsToAdd:    toAdd,
		MeetingsToUpdate: toUpdate,
		MeetingsToDelete: toDelete,
	}, nil
}

func (r *queryResolver) Meetings(ctx context.Context) ([]*model.Meeting, error) {
	return r.GetMeetings(), nil
}

func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }
func (r *Resolver) Query() generated.QueryResolver       { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
