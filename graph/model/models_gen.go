// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

type DeleteMeeting struct {
	ID int `json:"id"`
}

type Meeting struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	StartTime   *int   `json:"startTime"`
}

type NewMeeting struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	StartTime   *int   `json:"startTime"`
}

type UpdateMeeting struct {
	ID          int     `json:"id"`
	Title       *string `json:"title"`
	Description *string `json:"description"`
	StartTime   *int    `json:"startTime"`
}

type UpdateMeetingsInput struct {
	MeetingsToAdd    []*NewMeeting    `json:"meetingsToAdd"`
	MeetingsToUpdate []*UpdateMeeting `json:"meetingsToUpdate"`
	MeetingsToDelete []*DeleteMeeting `json:"meetingsToDelete"`
}

type UpdateMeetingsResponse struct {
	MeetingsToAdd    []*Meeting `json:"meetingsToAdd"`
	MeetingsToUpdate []*Meeting `json:"meetingsToUpdate"`
	MeetingsToDelete []*Meeting `json:"meetingsToDelete"`
}
