module github.com/dancemystyle/backendchallenge

go 1.13

require (
	github.com/99designs/gqlgen v0.11.1
	github.com/pkg/errors v0.8.1
	github.com/vektah/gqlparser/v2 v2.0.1
)
